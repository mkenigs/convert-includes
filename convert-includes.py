#!/usr/bin/env python3

import collections
import os
import re
import subprocess
import yaml

repos_to_branches = {
    'scratch-pipelines': ['rhel6', 'rhel7', 'rhel8', 'rhel9', 'rhel6-rt', 'rhel7-rt', 'rhel8-rt', 'rhel9-rt'],
    'external-triggers': ['ark', 'arm', 'block', 'bpf', 'kvm', 'mainline.kernel.org', 'net-next', 'rdma', 'rhel7', 'rhel8', 'rt-devel', 'scsi', 'umb-results', 'upstream-stable'],
    'brew-builds': ['ark', 'fedora', 'kernel-rt-rhel7', 'kernel-rt-rhel8', 'kernel-rt-rhel9', 'rhel7', 'rhel8', 'rhel9'],
    'cki-trusted-contributors': ['ark', 'arm', 'block', 'bpf', 'kernel-ark', 'kvm', 'mainline.kernel.org', 'net-next', 'rdma', 'rt-devel', 'scsi', 'upstream-stable'],
    'cki-internal-contributors': ['rhel6', 'rhel6-rt', 'rhel7', 'rhel7-rt', 'rhel8', 'rhel8-rt', 'rhel9', 'rhel9-rt'],
    'cki-public-pipelines': ['kernel-ark']
}

# TODO only use one dictionary
repos_to_commit_message = {
    'external-triggers': 'Externally-triggered pipeline',
    'brew-builds': 'Pipeline for Brew builds',
    'cki-trusted-contributors': 'Internal regular pipeline',
    'cki-public-pipelines': 'Public pipeline'
}

repos_to_project = {
    'external-triggers': 'cki-internal-pipelines/external-triggers.git',
    'brew-builds': 'cki-internal-pipelines/brew-builds.git',
    'cki-trusted-contributors': 'cki-internal-pipelines/cki-trusted-contributors.git',
    'cki-public-pipelines': 'cki-public-pipelines.git'
}

for repo, branches in repos_to_branches.items():
    os.chdir(repo)
    subprocess.run(['git', 'remote', 'add', 'cki-bot', f'https://cki-bot:{os.environ["CKI_BOT_TOKEN"]}@gitlab.com/redhat/red-hat-ci-tools/kernel/{repos_to_project[repo]}'])
    for branch in branches:
        subprocess.run(['git', 'checkout', branch], check=True)
        with open('.gitlab-ci.yml', 'r+') as gitlab_ci_yml:
            gitlab_ci_dict = yaml.safe_load(gitlab_ci_yml)
            if 'file' not in gitlab_ci_dict['include'][0]:
                tree_include = [include for include in gitlab_ci_dict['include'] if 'trees' in include][0]
                pipeline_include = [include for include in gitlab_ci_dict['include'] if 'trees' not in include][0]
                expected_pipeline_include = 'https://gitlab.com/cki-project/pipeline-definition/raw/main/cki_pipeline.yml'
                if pipeline_include != expected_pipeline_include:
                    raise Exception('Expected include: {expected_pipeline_include}')
                tree_yaml_name = re.search('raw/main/trees/(.*).yml', tree_include).group(1)
                out = {
                    'include': [
                        {
                            'project': 'cki-project/pipeline-definition',
                            'file': 'cki_pipeline.yml'
                        },
                        {
                            'project': 'cki-project/pipeline-definition',
                            'file': f'trees/{tree_yaml_name}.yml'
                        }
                    ]
                }
                gitlab_ci_yml.seek(0)
                gitlab_ci_yml.truncate()
                yaml.dump(out, gitlab_ci_yml)
                commit_message = repos_to_commit_message[repo]
                env = os.environ.copy()
                env['GIT_COMMITTER_NAME'] = 'CKI Bot'
                env['GIT_COMMITTER_EMAIL'] = '3953733-cki-bot@users.noreply.gitlab.com'
                subprocess.run(['git', 'commit', '-m', commit_message, '.gitlab-ci.yml', '--author="CKI Bot <3953733-cki-bot@users.noreply.gitlab.com>"', '--no-gpg-sign'], env=env, check=True)
                subprocess.run(['git', 'push', 'cki-bot', branch], check=True)
    os.chdir('..')
